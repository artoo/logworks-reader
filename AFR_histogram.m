function H = AFR_histogram (AFR, RPM, TPS, RPM_centers, TPS_centers)
    if ~exist('RPM_centers', 'var')
        RPM_centers = 0:1000:10000;
    end
    if ~exist('TPS_centers', 'var')
        TPS_centers = 0:10:100;
    end
    
    RPM_indices = findClosestBin(RPM, RPM_centers);
    TPS_indices = findClosestBin(TPS, TPS_centers);
    
    numTPSBins = length(TPS_centers);
    numRPMBins = length(RPM_centers);
    numSamples = length(AFR);
    
    binSamples = zeros(numTPSBins, numRPMBins);
    maxAFR = zeros(numTPSBins, numRPMBins);
    minAFR = zeros(numTPSBins, numRPMBins);
    avgAFR = zeros(numTPSBins, numRPMBins);
    stdAFR = zeros(numTPSBins, numRPMBins);
    
    for i = 1:numTPSBins
        for j = 1:numRPMBins
            indices = (RPM_indices == j) & (TPS_indices == i);
            AFRs = AFR(indices);
            if isempty(AFRs)
                continue;
            end
            binSamples(i, j) = length(AFRs);
            maxAFR(i, j) = max(AFRs);
            minAFR(i, j) = min(AFRs);
            avgAFR(i, j) = mean(AFRs);
            stdAFR(i, j) = std(AFRs);
        end
    end
    
    % TODO: brute force method
    %{
    for i = 1:numSamples
        index = sub2ind([numTPSBins numRPMBins], TPS_indices(i), RPM_indices(i));
        binSamples(index) = binSamples(index) + 1;
        maxAFR(index) = max(maxAFR(index), AFR(i));
        minAFR(index) = min(minAFR(index), AFR(i));
        avgAFR(index) = avgAFR(index) + AFR(i);
        stdAFR(index) = stdAFR(index) + AFR(i).^2;
    end
    
    nonZeroIndices = find(binSamples);
    avgAFR(nonZeroIndices) = avgAFR(nonZeroIndices) ./ binSamples(nonZeroIndices);
    stdAFR(nonZeroIndices) = sqrt((binSamples(nonZeroIndices) ./ (binSamples(nonZeroIndices) - 1)) .* (stdAFR(nonZeroIndices) ./ binSamples(nonZeroIndices) - avgAFR(nonZeroIndices).^2));
    %}
    
    H = struct( ...
        'n', binSamples, ...
        'avg', avgAFR, ...
        'std', stdAFR, ...
        'max', maxAFR, ...
        'min', minAFR);
end