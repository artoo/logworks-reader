#!/usr/bin/bash

if [ $# -lt 1 ]; then
    echo >&2 "Usage: $0 <log_dir>"
    exit -1
fi

LOG_DIR="$1"
COMB_LOG="$LOG_DIR/combined.csv"

rm -f "$COMB_LOG"
touch "$COMB_LOG".tmp

for log in $(ls "$LOG_DIR"/*.csv); do
    echo "Processing $log ..."
    sed -e 's/^\([^0-9].*\)/# \1/' -e 's/,/\t/g' "$log" >> "$COMB_LOG".tmp
done

/bin/mv "$COMB_LOG".tmp "$COMB_LOG"
