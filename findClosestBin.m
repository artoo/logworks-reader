function indices = findClosestBin (vals, centers)
    numVals = length(vals);
    numBins = length(centers);
    diff = repmat(vals(:), [1 numBins]) - repmat(centers(:)', [numVals 1]);
    [~, indices] = min(abs(diff), [], 2);
end